const mongoose = require("mongoose");
const ProductSchema = require("../modals/Product");
const HttpError = require("../modals/http-error");

const url = `mongodb+srv://parthadmin:Dga8KlISs1Qy9V7o@cluster0.q1qkrp6.mongodb.net/testMongo?retryWrites=true&w=majority`;

const connection = async () => {
    try {
        await mongoose.connect(url);
    } catch (error) {
        console.log("db connection error", error);
    }
};

const createProduct = async (req, res, next) => {
    try {
        const newProduct = new ProductSchema({
            productName: req.body.productName,
            price: req.body.price,
        });

        const result = await newProduct.save();
        console.log("create product result", result);

        res.json(result);
    } catch (error) {
        throw new HttpError(error.message);
    }
};

const getProducts = async (req, res, next) => {
    const result = await ProductSchema.find().exec();

    res.json(result);
};

module.exports.connection = connection;
module.exports.createProduct = createProduct;
module.exports.getProducts = getProducts;
