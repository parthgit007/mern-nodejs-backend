const MongoClient = require("mongodb").MongoClient;
const url = `mongodb+srv://parthadmin:Dga8KlISs1Qy9V7o@cluster0.q1qkrp6.mongodb.net/testMongo?retryWrites=true&w=majority`;
const client = new MongoClient(url);

const createProduct = async (req, res, next) => {
    const newProduct = {
        name: req.body.name,
        price: req.body.price,
    };

    try {
        await client.connect();
        const db = client.db();
        const result = await db.collection("testMongoDB").insertOne(newProduct);
        console.log("result", result);
    } catch (error) {
        console.log("error", error);
        return res.json({ message: "Could not store data" });
    }
    client.close();
    return res.json(newProduct);
};

const getProducts = async (req, res, next) => {
    let result = [];
    try {
        await client.connect();
        const db = client.db();
        result = await db.collection("testMongoDB").find().toArray();
        console.log("result", result);
    } catch (error) {
        console.log("error", error);
        return res.json({ message: "Could not store data" });
    }
    client.close();
    return res.json(result);  
};

module.exports.createProduct = createProduct;
module.exports.getProducts = getProducts;
