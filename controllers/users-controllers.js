const HttpError = require("../modals/http-error");
const UserSchema = require("../modals/User");
const { validationResult } = require("express-validator");

const DUMMY_USER = [
    {
        id: "u1",
        name: "Parth",
        email: "test@test.com",
        password: "test1234",
    },
];

const getAllUsers = async (req, res, next) => {
    let userList;

    try {
        userList = await UserSchema.find({}, '-password');
    } catch (error) {
        return next(new HttpError("Error while fetching the user list", 500));
    }

    res.json({
        users: userList.map((user) => user.toObject({ getters: true })),
    });
};

const userSignup = async (req, res, next) => {
    const error = validationResult(req);
    console.log("Error", error);
    if (!error.isEmpty()) {
        console.log("userSignup", error);
        return next(
            new HttpError(
                "Can not proceed further due to invalid input values",
                422
            )
        );
    }

    const { name, email, password, image } = req.body;

    let userEmail;
    try {
        userEmail = await UserSchema.findOne({ email });
    } catch (error) {
        const newError = new HttpError("Signup Failed, Please try again", 500);
        return next(newError);
    }

    if (userEmail) {
        return next(
            new HttpError("User already Exist, Try with different email"),
            500
        );
    }

    const createUser = new UserSchema({
        name,
        email,
        password,
        image,
        places:[]
    });

    try {
        await createUser.save();
    } catch (error) {
        return next(
            new HttpError(
                "Signup Failed, Something went wrong while user creation",
                500
            )
        );
    }

    res.status(201).json({
        message: "User successfully signup",
        user: createUser.toObject({ getters: true }),
    });
};

const userLogin = (req, res, next) => {
    const error = validationResult(req);
    if (!error.isEmpty()) {
        return next(
            new HttpError(
                "Can not proceed further due to invalid input values",
                422
            )
        );
    }
    const { email, password } = req.body;

    let userDetails;
    try {
        userDetails = UserSchema.findOne({
            email,
            password,
        });
    } catch (error) {
        return next(new HttpError("User not found, Please try again"), 404);
    }

    res.status(200).json({
        message: "User successfully loggedIn",
    });
};

exports.getAllUsers = getAllUsers;
exports.userSignup = userSignup;
exports.userLogin = userLogin;
