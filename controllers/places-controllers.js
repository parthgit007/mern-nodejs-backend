const HttpError = require("../modals/http-error");
const PlaceSchema = require("../modals/Place");
const { validationResult } = require("express-validator");
const User = require("../modals/User");
const { default: mongoose } = require("mongoose");

const DUMMY_PLACES = [
    {
        id: "u1",
        title: "New York",
        description: "Worlds most popular city",
        imageUrl:
            "https://upload.wikimedia.org/wikipedia/commons/7/7a/View_of_Empire_State_Building_from_Rockefeller_Center_New_York_City_dllu_%28cropped%29.jpg",
        address: "20 W 34th St., New York, NY 10001, United States",
        location: {
            lat: 40.7484405,
            lng: -73.9878531,
        },
        creator: "u1",
    },
    {
        id: "u2",
        title: "Ontario CN Tower",
        description: "Worlds most popular city",
        imageUrl:
            "https://lh5.googleusercontent.com/p/AF1QipOvilsftYr2X6e-8z6waS9wB7vavakovMIO7hs=w408-h543-k-no",
        address: "290 Bremner Blvd, Toronto, ON M5V 3L9, Canada",
        location: {
            lat: 43.6425701,
            lng: -79.3892455,
        },
        creator: "u2",
    },
    {
        id: "u3",
        title: "Mumbai Marin Drive",
        description: "Indian's Finiancial Capital",
        imageUrl:
            "https://lh5.googleusercontent.com/p/AF1QipOvilsftYr2X6e-8z6waS9wB7vavakovMIO7hs=w408-h543-k-no",
        address: "290 Bremner Blvd, Toronto, ON M5V 3L9, Canada",
        location: {
            lat: 43.6425701,
            lng: -79.3892455,
        },
        creator: "u3",
    },
];

const getPlaceById = async (req, res, next) => {
    const placeId = req.params.pid;
    console.log("placeId", placeId);
    let placeData;
    try {
        placeData = await PlaceSchema.findById(placeId);
    } catch (error) {
        const newError = new HttpError(
            "Something went wrong, Could not find the place",
            500
        );

        return next(newError);
    }

    console.log("PlaceData", placeData);

    if (!placeData) {
        const newError = new HttpError(
            "Could not find place for the provided Place ID",
            404
        );

        return next(newError);
    }

    res.json({
        place: placeData.toObject({ getters: true }),
    });
};

const getPlacesByUserId = async (req, res, next) => {
    const userId = req.params.uid;
    let userPlace;
    try {
        userPlace = await PlaceSchema.find({ userId: userId });
    } catch (error) {
        const newError = new HttpError(
            "Something went wrong, could not find the place for given userId",
            404
        );

        return next(newError);
    }

    if (!userPlace || userPlace.length === 0) {
        return next(
            new HttpError(
                `Could not find place for the provided userID ${userId}`,
                404
            )
        );
    }

    res.json({
        userPlace: userPlace.map((item) => item.toObject({ getters: true })),
    });
};

const createPlace = async (req, res, next) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        console.log(errors);
        const newError = new HttpError(
            "Can not proceed further due to invalid input values",
            422
        );
        return next(newError);
    }
    const { userId, title, description, address, location, creator } = req.body;

    const randomImageObject = DUMMY_PLACES.filter((item, index) => {
        if (index + 1 === Math.trunc(Math.random() * DUMMY_PLACES.length)) {
            return item.imageUrl;
        }
    });

    const newPlace = new PlaceSchema({
        userId: userId,
        title: title,
        description: description,
        address: address,
        location: location,
        creator: creator,
        image:
            randomImageObject.length > 0 ? randomImageObject[0].imageUrl : "",
    });

    let user;

    try {
        user = await User.findById(creator);
    } catch (error) {
        return next(
            new HttpError("Creating place is failed, Please try again", 500)
        );
    }

    if (!user) {
        const error = new HttpError("We could not find provided id ", 404);

        return next(error);
    }

    console.log("users", user);

    try {
        const session = await mongoose.startSession();
        session.startTransaction();
        await newPlace.save({ session: session });
        user.places.push(newPlace);
        await user.save({ session: session });
        await session.commitTransaction();
    } catch (error) {
        console.log(error);
        const errorRes = new HttpError("Can not create the places", 500);
        return next(errorRes);
    }

    res.status(201).json({ place: newPlace });
};

const updatePlace = async (req, res, next) => {
    const error = validationResult(req);
    if (!error.isEmpty()) {
        console.log("Update Place error", error);
        throw new HttpError(
            "Can not proceed further due to invalid input values",
            422
        );
    }
    const { id, title, description, creator } = req.body;
    console.log("id", id);

    try {
        const result = await PlaceSchema.findByIdAndUpdate(id, {
            title: "Hello Parth",
            description:
                "You can do anything just believe and start acting instead of thinking",
        }).exec();
        console.log("update result", result);
    } catch (error) {
        console.log("error", error);
    }

    res.status(200).json({
        message: `Place ${id} updated successfully`,
        place: updatePlace,
    });
};

const deletePlace = async (req, res, next) => {
    const pid = req.params.pid;
    console.log("pid", pid);

    let place;
    try {
        place = await PlaceSchema.findById(pid).populate("creator");
    } catch (error) {
        console.log("errors", error);
        const newError = new HttpError(
            "Something went wrong while finding the place for given Id",
            500
        );

        return next(newError);
    }

    if (!place) {
        const newError = new HttpError(
            "Can not find the place for given Id",
            404
        );

        return next(newError);
    }

    try {
        const session = await mongoose.startSession();
        session.startTransaction();
        await place.deleteOne({ session: session });
        place.creator.places.pull(place);
        await place.creator.save({session: session});
        session.commitTransaction();
    } catch (error) {
        console.log("errors", error);
        const newError = new HttpError(
            "Something went wrong while deleting the place",
            500
        );

        return next(newError);
    }

    res.status(200).json({
        message: `Record Deleted successfully`,
        updatedPlaces: updatePlace,
    });
};

exports.getPlaceById = getPlaceById;
exports.getPlacesByUserId = getPlacesByUserId;
exports.createPlace = createPlace;
exports.updatePlace = updatePlace;
exports.deletePlace = deletePlace;
