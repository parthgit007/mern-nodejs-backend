const express = require("express");
const userController = require("../controllers/users-controllers");
const { check } = require("express-validator");
const router = express.Router();

router.get("/", userController.getAllUsers);
router.post(
    "/signup",
    [
        check("email").normalizeEmail().notEmpty(),
        check("name").notEmpty(),
        check("password").isLength({ min: 6 }),
    ],
    userController.userSignup
);
router.post(
    "/login",
    [
        check("email").normalizeEmail().notEmpty(),
        check("password").isLength({ min: 6 }),
    ],
    userController.userLogin
);

module.exports = router;
