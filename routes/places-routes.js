const express = require("express");
const { check } = require("express-validator");
const router = express.Router();

const placeController = require("../controllers/places-controllers");

// Routes
router.get("/:pid", placeController.getPlaceById);
router.get("/user/:uid", placeController.getPlacesByUserId);
router.patch(
    "/:pid",
    [
        check("title").notEmpty(),
        check("description").notEmpty(),
        check("creator").notEmpty(),
    ],
    placeController.updatePlace
);
router.delete("/:pid", placeController.deletePlace);
router.post(
    "/",
    [
        check("title").notEmpty(),
        check("description").isLength({ min: 5 }),
        check("address").notEmpty(),
    ],
    placeController.createPlace
);

module.exports = router;
