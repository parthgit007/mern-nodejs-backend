const express = require("express");
const bodyParser = require("body-parser");

const placesRoutes = require("./routes/places-routes");
const usersRoutes = require("./routes/users-routes");
// const createProduct = require("./db/mongo").createProduct;
// const getProducts = require("./db/mongo").getProducts;
const createProduct = require("./db/mongoose").createProduct;
const getProducts = require("./db/mongoose").getProducts;
const connection = require("./db/mongoose").connection;
const HttpError = require("./modals/http-error");
const app = express();

app.use(bodyParser.json());
app.use("/api/places", placesRoutes);
app.use("/api/users", usersRoutes);
app.post("/api/mongoTest", createProduct);
app.get("/api/mongoTest/products", getProducts);

app.use("/health-check", (req, res, next) => {
    res.status(200).send("Server health is in good state", 200);
});

app.use((req, res, next) => {
    return next(new HttpError("Could not find this route", 404));
});

app.use((error, req, res, next) => {
    if (res.headerSent) {
        return next(error);
    }
    res.status(error.code || 500);
    res.json({
        message: error.message || "An unknown error occurred ",
    });
    console.log(error);
});

const makeDbConnection = async () => {
    await connection()
        .then(() => {
            console.log("DB connection successfully");
            app.listen(5000, () => {
                console.log(`Server is listing on 5000`);
            });
            
        })
        .catch((error) => {
            console.log("Db Connection error : --->", error);
        });
};
makeDbConnection();

