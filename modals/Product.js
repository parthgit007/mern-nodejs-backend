const mongoose = require('mongoose');
const { Schema } = mongoose;

const ProductSchema = new Schema({
    productName: {type: String, required: true},
    price: Number,
});

module.exports = mongoose.model('Product', ProductSchema);
