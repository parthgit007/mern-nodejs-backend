const mongoose = require("mongoose");
const User = require("./User");

const { Schema } = mongoose;

const PlaceSchema = new Schema({
    title: { type: String, require: true },
    description: { type: String, require: true },
    address: { type: String, require: true },
    userId: { type: String, require: true },
    location: {
        type: {
            lat: { type: Number, require: true },
            lng: { type: Number, require: true },
        },
        require: true,
    },
    image: { type: String, require: true },
    // creator: { type: String, require: true },
    creator: { type: mongoose.Types.ObjectId, required: true, ref: User},
});

module.exports = mongoose.model("Place", PlaceSchema);
